using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simple3DPathfinding : MonoBehaviour
{
    public Transform startNode;
    public Transform targetNode;
    public LayerMask obstacleMask;

    private Dictionary<Transform, Transform> cameFrom = new Dictionary<Transform, Transform>();

    void Start()
    {
        // ��������� �������� ������ ���� � ������
        BFSAlgorithm(startNode, targetNode);
    }

    void BFSAlgorithm(Transform start, Transform target)
    {
        Queue<Transform> queue = new Queue<Transform>();
        queue.Enqueue(start);

        while (queue.Count > 0)
        {
            Transform current = queue.Dequeue();

            if (current == target)
            {
                // ���� ������, ������� �� �����
                break;
            }

            foreach (Transform neighbor in GetNeighbors(current))
            {
                if (!cameFrom.ContainsKey(neighbor))
                {
                    cameFrom[neighbor] = current;
                    queue.Enqueue(neighbor);
                }
            }
        }

        // ��������������� ����
        List<Transform> path = ReconstructPath(start, target);
        // ���������� ���������� ����
        UsePath(path);
    }

    List<Transform> GetNeighbors(Transform node)
    {
        List<Transform> neighbors = new List<Transform>();

        // �������� ������� ������� �������
        Collider[] colliders = Physics.OverlapSphere(node.position, 1f, obstacleMask);
        foreach (Collider collider in colliders)
        {
            Transform neighbor = collider.transform;
            neighbors.Add(neighbor);
        }

        return neighbors;
    }

    List<Transform> ReconstructPath(Transform start, Transform target)
    {
        List<Transform> path = new List<Transform>();
        Transform current = target;

        while (current != start)
        {
            path.Add(current);
            if (cameFrom.ContainsKey(current))
            {
                current = cameFrom[current];
            }
            else
            {
                // ��� ���� �� ����
                return null;
            }
        }

        path.Reverse();
        return path;
    }

    void UsePath(List<Transform> path)
    {
        if (path != null)
        {
            foreach (Transform node in path)
            {
                // ��������, ����������� �� ������ ���� ����
                Debug.Log("Node: " + node.name);
            }
        }
        else
        {
            // ���� �� ������
            Debug.Log("Path not found!");
        }
    }
}