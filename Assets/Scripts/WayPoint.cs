using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour
{
    public bool isExplored = false;
    public WayPoint exploredFrom;
    const int gridSize = 10;
    Vector2Int gridPos;

    public int GetGridSize()
    {
        return gridSize;
    }

    public Vector2Int GetGridPos()
    {
        gridPos.x = Mathf.RoundToInt(transform.position.x / gridSize);
        gridPos.y = Mathf.RoundToInt(transform.position.z / gridSize);
        return new Vector2Int(gridPos.x, gridPos.y);
    }

    public void SetTopColor(Color color)
    {
        //MeshRenderer topMeshRenderer = transform.Find("TopQuad").GetComponent<MeshRenderer>();
        //topMeshRenderer.material.color = color;
        transform.GetComponent<MeshRenderer>().material.color = color;
    }
}
