using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sorting : MonoBehaviour
{
    public int[] numbers = { 78, 55, 45, 98, 13 };
    void Start()
    {
        SelectionSort(numbers);
        Debug.Log("Sorted numbers :"+string.Join(",", numbers));
    }

    public void SelectionSort(int[] nums)
    {
        for (int i = 0; i < nums.Length - 1; i++)
        {
            int smallestNumIndex = i;
            for (int j = i; j < nums.Length; j++)
            {
                if (nums[j] < nums[smallestNumIndex])
                {
                    smallestNumIndex = j;
                }
            }
            if(smallestNumIndex != i)
            {
                int temp = nums[i];
                nums[i] = nums[smallestNumIndex];
                nums[smallestNumIndex] = temp;
            }
        }
    }
}
