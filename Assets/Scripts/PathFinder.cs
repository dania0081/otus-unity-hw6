using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
    [SerializeField] WayPoint startWaypoint, endWaypoint;
    Dictionary<Vector2Int, WayPoint> grid = new Dictionary<Vector2Int, WayPoint>();
    Queue<WayPoint> queue = new Queue<WayPoint>();

    bool isRunning = true;
    WayPoint searchCenter;

    Vector2Int[] directions =
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    private void Start()
    {
        LoadBlocks();
        BreadthFirstSearch();
        ColorPath();
    }
    
    public void Find()
    {
        LoadBlocks();
        BreadthFirstSearch();
        ColorPath();
    }

    private void BreadthFirstSearch()
    {
        queue.Enqueue(startWaypoint);

        while (queue.Count > 0 && isRunning)
        {
            searchCenter = queue.Dequeue();
            HaltIfEndRound();
            ExploreNeighbours();
            searchCenter.isExplored = true;
        }
    }

    private void HaltIfEndRound()
    {
        if (searchCenter == endWaypoint)
        {
            isRunning = false;
        }
    }

    private void ExploreNeighbours()
    {
        if (!isRunning) { return; }

        foreach (Vector2Int direction in directions)
        {
            Vector2Int neighbourCoordinates = searchCenter.GetGridPos() + direction;
            if (grid.ContainsKey(neighbourCoordinates))
            {
                QueueNewNeighbours(neighbourCoordinates);
            }
        }
    }

    private void QueueNewNeighbours(Vector2Int neighbourCoordinates)
    {
        WayPoint neighbour = grid[neighbourCoordinates];

        if (!neighbour.isExplored && !queue.Contains(neighbour))
        {
            neighbour.SetTopColor(Color.blue);
            queue.Enqueue(neighbour);
            neighbour.exploredFrom = searchCenter;
        }
    }

    private void LoadBlocks()
    {
        WayPoint[] wayPoints = FindObjectsOfType<WayPoint>();
        foreach (WayPoint wayPoint in wayPoints)
        {
            if (grid.ContainsKey(wayPoint.GetGridPos()))
            {
                Debug.Log("��������� ���������� ����");
            }
            else
            {
                grid.Add(wayPoint.GetGridPos(), wayPoint);
            }
        }
    }

    private void ColorPath()
    {
        ColorTheTrail();
        startWaypoint.SetTopColor(Color.green);
        endWaypoint.SetTopColor(Color.red);
    }

    private void ColorTheTrail()
    {
        WayPoint paintedWaypoint = searchCenter.exploredFrom;
        while (paintedWaypoint != startWaypoint)
        {
            paintedWaypoint.SetTopColor(Color.grey);
            paintedWaypoint = paintedWaypoint.exploredFrom;
        }
    }
}
